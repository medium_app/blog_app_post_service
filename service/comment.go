package service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	pb "gitlab.com/medium_app/blog_app_post_service/genproto/post_service"
	"gitlab.com/medium_app/blog_app_post_service/storage"
	"gitlab.com/medium_app/blog_app_post_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CommentService struct {
	pb.UnimplementedCommentServiceServer
	storage storage.StorageI
}

func NewCommentService(strg storage.StorageI) *CommentService {
	return &CommentService{
		storage: strg,
	}
}

func (s *CommentService) Create(ctx context.Context, req *pb.Comment) (*pb.Comment, error) {
	comment, err := s.storage.Comment().Create(&repo.Comment{
		PostID:      req.PostId,
		UserID:      req.UserId,
		Description: req.Description,
	})
	if err != nil {
		fmt.Println("create comment error")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseCommentModel(comment), nil
}

func (s *CommentService) GetAll(ctx context.Context, req *pb.GetAllCommentsRequest) (*pb.GetAllCommentsResponse, error) {
	result, err := s.storage.Comment().GetAll(&repo.GetCommentsParams{
		Limit:  req.Limit,
		Page:   req.Page,
		PostID: req.PostId,
		UserID: req.UserId,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to get all comments: %v", err)
	}

	response := pb.GetAllCommentsResponse{
		Comments: make([]*pb.Comment, 0),
		Count:    result.Count,
	}

	for _, comment := range result.Comments {
		response.Comments = append(response.Comments, parseCommentModel(comment))
	}

	return &response, nil
}

func (s *CommentService) Update(ctx context.Context, req *pb.Comment) (*emptypb.Empty, error) {
	err := s.storage.Comment().Update(&repo.Comment{
		ID:          req.Id,
		Description: req.Description,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to update a comment: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *CommentService) Delete(ctx context.Context, req *pb.GetCommentRequest) (*emptypb.Empty, error) {
	err := s.storage.Comment().Delete(req.Id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to delete a comment: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func parseCommentModel(comment *repo.Comment) *pb.Comment {
	return &pb.Comment{
		Id:          comment.ID,
		PostId:      comment.PostID,
		UserId:      comment.UserID,
		Description: comment.Description,
		CreatedAt:   comment.CreatedAt.Format(time.RFC3339),
		UpdatedAt:   comment.UpdatedAt.Format(time.RFC3339),
	}
}
