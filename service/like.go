package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/medium_app/blog_app_post_service/genproto/post_service"
	"gitlab.com/medium_app/blog_app_post_service/storage"
	"gitlab.com/medium_app/blog_app_post_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type LikeService struct {
	pb.UnimplementedLikeServiceServer
	storage storage.StorageI
}

func NewLikeService(strg storage.StorageI) *LikeService {
	return &LikeService{
		storage: strg,
	}
}

func (s *LikeService) CreateOrUpdate(ctx context.Context, req *pb.Like) (*emptypb.Empty, error) {
	err := s.storage.Like().CreateOrUpdate(&repo.Like{
		PostID: req.PostId,
		UserID: req.UserId,
		Status: req.Status,
	})

	if err != nil {
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *LikeService) Get(ctx context.Context, req *pb.GetLikeRequest) (*pb.Like, error) {
	like, err := s.storage.Like().Get(req.PostId, req.UserId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get like: %v", err)
	}

	return parseLikeModel(like), nil
}

func (s *LikeService) GetLikesDislikesCount(ctx context.Context, req *pb.LikesDislikesCountRequest) (*pb.LikesDislikesCountResponse, error) {
	result, err := s.storage.Like().GetLikesDislikesCount(req.PostId)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to get likes and dislikes count: %v", err)
	}

	return &pb.LikesDislikesCountResponse{
		LikesCount:    result.LikesCount,
		DislikesCount: result.DislikesCount,
	}, nil
}

func parseLikeModel(like *repo.Like) *pb.Like {
	return &pb.Like{
		Id:     like.ID,
		PostId: like.PostID,
		UserId: like.UserID,
		Status: like.Status,
	}
}
