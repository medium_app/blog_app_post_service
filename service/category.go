package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	pb "gitlab.com/medium_app/blog_app_post_service/genproto/post_service"
	"gitlab.com/medium_app/blog_app_post_service/storage"
	"gitlab.com/medium_app/blog_app_post_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CategoryService struct {
	pb.UnimplementedCategoryServiceServer
	storage storage.StorageI
}

func NewCategoryService(strg storage.StorageI) *CategoryService {
	return &CategoryService{
		storage: strg,
	}
}

func (s *CategoryService) Create(ctx context.Context, req *pb.Category) (*pb.Category, error) {
	category, err := s.storage.Category().Create(&repo.Category{
		Title: req.Title,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseCategoryModel(category), nil
}

func (s *CategoryService) Get(ctx context.Context, req *pb.GetCategoryRequest) (*pb.Category, error) {
	category, err := s.storage.Category().Get(req.Id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get a category: %v", err)
	}

	return parseCategoryModel(category), nil
}

func (s *CategoryService) GetAll(ctx context.Context, req *pb.GetAllCategoriesRequest) (*pb.GetAllCategoriesResponse, error) {
	result, err := s.storage.Category().GetAll(&repo.GetCategoriesParams{
		Limit:  req.Limit,
		Page:   req.Page,
		Search: req.Search,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to get all categories: %v", err)
	}

	response := pb.GetAllCategoriesResponse{
		Categories: make([]*pb.Category, 0),
		Count:      result.Count,
	}

	for _, category := range result.Categories {
		response.Categories = append(response.Categories, parseCategoryModel(category))
	}

	return &response, nil
}

func (s *CategoryService) Update(ctx context.Context, req *pb.Category) (*pb.Category, error) {
	category, err := s.storage.Category().Update(&repo.Category{
		ID:    req.Id,
		Title: req.Title,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to update a category: %v", err)
	}

	return parseCategoryModel(category), nil
}

func (s *CategoryService) Delete(ctx context.Context, req *pb.GetCategoryRequest) (*emptypb.Empty, error) {
	err := s.storage.Category().Delete(req.Id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to delete a category: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func parseCategoryModel(category *repo.Category) *pb.Category {
	return &pb.Category{
		Id:        category.ID,
		Title:     category.Title,
		CreatedAt: category.CreatedAt.Format(time.RFC3339),
	}
}
